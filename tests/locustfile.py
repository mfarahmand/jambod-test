import json
import random

import gevent
from locust import TaskSet, task, HttpLocust, between


class UserBehaviour(TaskSet):
    username_length = 16
    confirmation_period = 2

    def on_start(self):
        self.client.post('/sign-in/', {'username': 'admin', 'password': 'password'})
        # The client will be logged in and redirected after a successful signup.

    @task
    def order_a_car(self):
        response = self.client.get(f'/available/')
        available_cars = response.json()
        chosen_car = random.choice(list(available_cars.keys()))
        try:
            # The client must first place their order for a car.
            # Purchases are not immediately evaluated; they will be assigned a unique ID so that they can be tracked later.
            response = self.client.get(f'/order/{chosen_car}/')
            tracking_id = response.json().get('sell_id')
            # The frontend interface must regularly check for the confirmation of the purchase.
            # Purchases are queued and processed according to the capacity of the server. Therefore, validating a purchase
            # can be spontaneous (under light load and with high specs) or it might take a few seconds.
            # Here, I'll check with the backend every 2s and terminate the process once the purchase has been accepted or
            # denied.
            confirmed = False
            while not confirmed:
                gevent.sleep(self.confirmation_period)  # Only want the current thread to block.
                response = self.client.get(f'/status/{tracking_id}/')
                if response.json().get('status') != 0:
                    confirmed = True
        except json.decoder.JSONDecodeError:
            pass
            # In case of a Server Error, the responses won't contain a valid JSON structure. This block avoids printing
            # these exceptions, but the failures will be included in the error reports.

    def on_end(self):
        self.client.get('/sign-out')


class WebsiteUser(HttpLocust):
    task_set = UserBehaviour
    wait_time = 0
