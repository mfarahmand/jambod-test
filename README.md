Note: Certain security considerations and mechanisms were dismissed, since the focus of the test seemed to be on reliability. 
I'm well aware of them and you won't see them in a Django app developed by me out of this context :D

# The architecture of the Application

<div align="center">
    <img src="tests/architecture.png" alt="Architecture of the Website">
</div>

The most crucial model in the application is the **Sell** model. *Sell*s correspond to a particular vehicle and are made
by a specific user. They also record the date and time of making the purchase. When a user makes a purchase, the request
is not stalled so that the system can check with the database to make sure that they are enough cars left in the stock.
Instead, they are simply added to a queue stored in **Redis** which is managed by **Django Q** for later processing. A 
tracking ID is assigned to each purchase and returned to the frontend. The webapp or the mobile app on the front can 
regularly check for the confirmation of the purchase.

# API Calls

-   Django's Sign-Up Form: `/sign-up/` 

    **POST** `username`, `password1`, `password2`, `email`
    
    400 (Bad Request) in case of an error, otherwise, redirects to the home page.
    
-   Login: `/sign-in/`

    **POST** `username`, `password`
    
    Results in 401 (Unauthorized) in case of an error, otherwise, redirects to the home page.
    
-   Logout: `/sign_out/`

    **GET** Terminates the active session.
    
-   Uploading a CSV file: `/upload/` (accessible only to staff users)

    **POST** `list`: CSV file with the same structure as in the sample file.
    
    Returns the number of new cars and the number of records successfully processed as a JSON response.

-   List of Available Cars: `/available/`

    **GET** Returns a JSON object, mapping IDs to car labels. Only the cars that haven't been sold out appear in the list.

-   Ordering a Car: `/order/{car_id}/`

    **GET** Places an order for the car with the specified ID for the user. The method returns a `sell_id` inside a JSON
    object which can be used to track the purchase. For an invalid `car_id`, the response would be 404.

-   Tracking a Purchase: `/status/{sell_id}/`

    **GET** Returns the `status` of the purchase with the designated `sell_id`. The status of a purchase can be:
    
    -   `0`: Pending; the request hasn't yet been processed by the scheduler.
    -   `1`: Successful.
    -   `-1`: Out of stock.
    
    A request with an invalid `sell_id` results in 404.


# Load Testing

The tests were conducted using the Python package, **Locust**. The configuration of the tests are stored in the `tests` 
directory. For the tests, the Django website was deployed using Nginx and uWSGI, running inside a VirtualBox virtual
machine, with a single CPU core (with no hyper-threading), 1024MB of RAM (half of which was occupied by the OS), and 
10GB of hard drive. The test made no delays between the requests, making the test harder than reality and pushing the
system to its very limits.

The system was able to sustain 1000 users (hatched at 10UPS) continuously logging in into an account, making and order,
and logging out after checking for the confirmation of their purchase for around 30s. A portion of the requests start 
failing shortly after. 

![Locust Webapp](tests/screenshot.png)

The inevitable conclusion of analyzing Nginx's `errors.log` and the systemd journals on uWSGI is that the limited number
of cores and available memory are not enough to handle a continuous stream of requests made by 1000 users 
simultaneously. Several logs were recorded by the system, among the which the two below were the most prominent:

-   Nginx Alert: `worker_connections are not enough`.
-   Nginx Failure: `too many open files` which was solved by increasing the `/proc/sys/fs/file-max` limit.
-   uWSGI Error for low number of backlogs, which was solved by increasing the `/proc/sys/net/core/somaxconn` limit and
then editing uWSGI `listen` option itself.

After tweaking the OS, Nginx, and uWSGI, the system can reach a state in which it can handle 1000 active users for 
prolonged amounts of time. However, the response times increase steadily, making the website unusable to the users. My
approach to resolving this issue is to give the system more resources, especially processing cores, so it can run 
multiple threads at the same time, and support high numbers of `inodes`. The designed architecture will be able to show
its true potential in such circumstances.
