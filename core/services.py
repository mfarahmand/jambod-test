from django.db.models import Sum

from . import models


def validate_sell(sell_id):
    sell = models.Sell.objects.get(id=sell_id)
    sold = models.Sell.objects.filter(car=sell.car, at__lte=sell.at, validated=True)
    aggr = models.Sale.objects.filter(car=sell.car, available_since__lte=sell.at).aggregate(Sum('stock'))
    sell.validated = sold.count() < (aggr['stock__sum'] or 0)
    sell.save()
