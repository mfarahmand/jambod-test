from django.db.models.signals import post_save
from django_q.tasks import async_task

from . import models


def sell_created(sender, instance, created, **kwargs):
    # Could have also overridden the `save` method on the model, but using signals felt like the more sophisticated approach!
    if created:
        async_task('core.services.validate_sell', instance.id)


post_save.connect(sell_created, sender=models.Sell)
