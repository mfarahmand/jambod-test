import datetime

import jdatetime


def string_to_date(string):
    """Transforms a string in `DD/MM/YYYY` format into a date. When `YYYY` surpasses 2000, the date is supposed to be in
    the Gregorian calendar, and otherwise, in Jalali.

    :param string: Date string in 'DD/MM/YYYY' format.
    :return: Naive date object.
    """
    day, month, year = (int(i) for i in string.split('/'))
    return datetime.date(year, month, day) if year > 2000 else jdatetime.date(year, month, day).togregorian()
