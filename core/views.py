import csv
import io

from django.http import HttpResponse, JsonResponse, HttpResponseBadRequest
from django.shortcuts import redirect, get_object_or_404
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm

from . import models, utils


def home(request):
    return HttpResponse()


def sign_up(request):
    if request.POST:
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
        return HttpResponseBadRequest()
    return HttpResponse()


def sign_in(request):
    if request.POST:
        user = authenticate(username=request.POST.get('username', ''), password=request.POST.get('password', ''))
        if user is not None:
            login(request, user)
            return redirect(request.GET.get('next') or 'home')
        return HttpResponse(status=401)
    return HttpResponse()


@login_required
def sign_out(request):
    logout(request)
    return redirect('home')


@login_required
def upload(request):
    if request.user.is_staff and request.FILES:
        if 'list' in request.FILES:
            file_contents = request.FILES.get('list').read().decode('utf-8')
            file_like_obj = io.StringIO(file_contents)
            csv_reader = csv.reader(file_like_obj)
            next(csv_reader)  # Skip the headers.
            total_rows = 0
            new_cars = 0
            for car_model, available_since, price, stock in csv_reader:
                car, created = models.Car.objects.get_or_create(model=car_model)
                if created:
                    new_cars += 1
                available_since = utils.string_to_date(available_since)
                price = int(price)
                stock = int(stock)
                models.Sale.objects.create(car=car, available_since=available_since, price=price, stock=stock)
                total_rows += 1
            return JsonResponse({'added': total_rows, 'new_cars': new_cars})
        return HttpResponseBadRequest()
    return HttpResponse()


@login_required
def available(request):
    return JsonResponse({car.id: car.model for car in models.Car.objects.order_by('id') if car.remaining()})


@login_required
def order(request, car):
    sell = models.Sell.objects.create(car=get_object_or_404(models.Car, id=car), by=request.user)
    return JsonResponse({'sell_id': sell.id})


@login_required
def status(request, sell):
    sell = get_object_or_404(models.Sell, id=sell)
    # Status values are 1 for successful purchases, -1 for failed purchases, and 0 for purchases that are yet to be processed.
    return JsonResponse({'status': 1 if sell.validated else -1 if sell.validated is False else 0})
