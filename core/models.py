from django.conf import settings
from django.db import models


class Car(models.Model):
    model = models.CharField(max_length=255, unique=True)

    def stock_sum(self):
        aggr = Sale.objects.filter(car=self).aggregate(models.Sum('stock'))
        return aggr['stock__sum'] or 0

    def sold_count(self):
        return Sell.objects.filter(car=self, validated=True).count()

    def remaining(self):
        return self.stock_sum() - self.sold_count()


class Sale(models.Model):
    car = models.ForeignKey(to=Car, on_delete=models.CASCADE)
    available_since = models.DateField()
    price = models.BigIntegerField()
    stock = models.PositiveSmallIntegerField()


class Sell(models.Model):
    car = models.ForeignKey(to=Car, on_delete=models.CASCADE)
    at = models.DateTimeField(auto_now=True)
    by = models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    validated = models.BooleanField(null=True)
