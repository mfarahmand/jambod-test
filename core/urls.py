from django.urls import path

from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('sign-up/', views.sign_up, name='sign-up'),
    path('sign-in/', views.sign_in, name='sign-in'),
    path('sign-out/', views.sign_out, name='sign-out'),
    path('upload/', views.upload, name='upload'),
    path('available/', views.available, name='available'),
    path('order/<int:car>/', views.order, name='order'),
    path('status/<int:sell>/', views.status, name='status')
]
